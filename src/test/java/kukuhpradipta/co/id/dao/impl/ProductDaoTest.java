package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.model.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Widyana K Pradipta on 11/24/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductDaoTest {

    @Autowired
    ProductDao productDao;


    @Test
    public void getCheapestProductByProductCodeTest() {
        //Positive Case
        String productCode = "E001";
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode));
        //positive case get value each variable
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getProduct());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getProductCode());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getDescription());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getPrice());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getToken());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getSupplierCode());

        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getPrice()  == 1750 );
        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getSupplierCode().equals("S001"));
        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getToken().equals("163yghjknqhwve61723"));

        productCode = "W001";
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode));
        //positive case get value each variable
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getProduct());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getProductCode());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getDescription());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getPrice());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getToken());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getSupplierCode());

        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getPrice()  == 1255 );
        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getSupplierCode().equals("S002"));
        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getToken().equals("0987192jahskdjasd"));

        productCode = "I001";
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode));
        //positive case get value each variable
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getProduct());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getProductCode());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getDescription());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getPrice());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getToken());
        Assert.assertNotNull(productDao.getCheapestProductByProductCode(productCode).getSupplierCode());

        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getPrice()  == 2500 );
        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getSupplierCode().equals("S001"));
        Assert.assertTrue(productDao.getCheapestProductByProductCode(productCode).getToken().equals("6789oghjaksjdaksdaaa"));
    }

    @Test
    public void getProductByProductCodeAndSupplierIdTest(){
        Product product = productDao.getProductByProductCodeAndSupplier("E001", "S001");
        Assert.assertNotNull(product);
        Assert.assertNotNull(product.getSupplierCode());
        Assert.assertNotNull(product.getProductCode());
        Assert.assertTrue(product.getSupplierCode().equals("S001"));
        Assert.assertTrue(product.getProductCode().equals("E001"));
        Assert.assertTrue(product.getProduct().equals("Electricity"));
    }
}
