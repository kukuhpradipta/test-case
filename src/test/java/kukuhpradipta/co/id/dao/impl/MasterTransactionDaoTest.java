package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.MasterTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.model.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class MasterTransactionDaoTest {

    @Autowired
    MasterTransactionDao masterTransactionDao;

    @Autowired
    ProductDao productDao;

    @Test
    public void saveTest(){
        TransactionData transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR222");
        Product product = productDao.getCheapestProductByProductCode(transactionData.getProduct());

        MasterTransaction masterTransaction = masterTransactionDao.save(transactionData.getTransactionId(),transactionData.getEmail(),product.getSupplierCode(),product.getToken(),"INQUIRE",product.getPrice(),transactionData.getNominal() + product.getPrice(), transactionData.getPassword(), transactionData.getProduct(), transactionData.getNominal());
        System.out.println("mastertransaction "+masterTransaction);
        Assert.assertNotNull(masterTransaction);
        Assert.assertTrue(masterTransaction.getSupplier().equals(product.getSupplierCode()));
        Assert.assertTrue(masterTransaction.getFee().equals(product.getPrice()));
        Assert.assertTrue(masterTransaction.getToken().equals(product.getToken()));
        Assert.assertNotNull(masterTransaction.getCreatedTimestamp());
        Assert.assertNotNull(masterTransaction.getUpdatedTimestamp());
        Assert.assertTrue(masterTransaction.getCreatedTimestamp().equals(masterTransaction.getUpdatedTimestamp()));

        transactionData = new TransactionData("W001", "testong@gmail.com", "test1234", 10000L, "TRW001");
        product = productDao.getCheapestProductByProductCode(transactionData.getProduct());


        masterTransaction = masterTransactionDao.save(transactionData.getTransactionId(),transactionData.getEmail(),product.getSupplierCode(),product.getToken(),"INQUIRE",product.getPrice(),transactionData.getNominal() + product.getPrice(), transactionData.getPassword(), transactionData.getProduct(), transactionData.getNominal());
        Assert.assertNotNull(masterTransaction);
        Assert.assertTrue(masterTransaction.getSupplier().equals(product.getSupplierCode()));
        Assert.assertTrue(masterTransaction.getFee().equals(product.getPrice()));
        Assert.assertTrue(masterTransaction.getToken().equals(product.getToken()));
        Assert.assertNotNull(masterTransaction.getCreatedTimestamp());
        Assert.assertNotNull(masterTransaction.getUpdatedTimestamp());
        Assert.assertTrue(masterTransaction.getCreatedTimestamp().equals(masterTransaction.getUpdatedTimestamp()));

    }

    @Test
    public void findByTransactionIdTest(){
        TransactionData transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR001");
        Product product = productDao.getCheapestProductByProductCode(transactionData.getProduct());
        masterTransactionDao.save(transactionData.getTransactionId(),transactionData.getEmail(),product.getSupplierCode(),product.getToken(),"INQUIRE",product.getPrice(),transactionData.getNominal() + product.getPrice(),transactionData.getPassword(), transactionData.getProduct(), transactionData.getNominal());

        MasterTransaction masterTransaction = masterTransactionDao.findByTransactionId(transactionData.getTransactionId());
        Assert.assertTrue(masterTransaction.getSupplier().equals(product.getSupplierCode()));
        Assert.assertTrue(masterTransaction.getFee().equals(product.getPrice()));
        Assert.assertTrue(masterTransaction.getToken().equals(product.getToken()));
        Assert.assertNotNull(masterTransaction.getCreatedTimestamp());
        Assert.assertNotNull(masterTransaction.getUpdatedTimestamp());
    }

    @Test
    public void updateTest(){
        TransactionData transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR877");
        Product product = productDao.getCheapestProductByProductCode(transactionData.getProduct());

        MasterTransaction masterTransaction = masterTransactionDao.save(transactionData.getTransactionId(),transactionData.getEmail(),product.getSupplierCode(),product.getToken(),"INQUIRE",product.getPrice(),transactionData.getNominal() + product.getPrice(), transactionData.getPassword(), transactionData.getProduct(), transactionData.getNominal());
        MasterTransaction masterTransaction2 = masterTransactionDao.findByTransactionId(masterTransaction.getTransactionId());
        masterTransaction2.setState("PAYMENT");
        masterTransaction2.setUpdatedTimestamp(new Timestamp(Calendar.getInstance().getTime().getTime()));
        masterTransactionDao.update(masterTransaction2);
        Assert.assertNotNull(masterTransaction2);
        Assert.assertTrue(!masterTransaction2.getState().equals(masterTransaction.getState()));
        Assert.assertTrue(masterTransaction.getCreatedTimestamp().before(masterTransaction2.getUpdatedTimestamp()));
        Assert.assertTrue(masterTransaction2.getUpdatedTimestamp().after(masterTransaction.getCreatedTimestamp()));
    }

}
