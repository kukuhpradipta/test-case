package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.DetailTransaction;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DetailTransactionDaoTest {

    @Autowired
    DetailTransactionDao detailTransactionDao;

    @Test
    public void saveTest(){
        TransactionData transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR001");
        DetailTransaction detailTransaction = detailTransactionDao.save(transactionData, "inquire");
        Assert.assertNotNull(detailTransaction);
        Assert.assertTrue(detailTransaction.getNominal().equals(transactionData.getNominal()));
        Assert.assertTrue(detailTransaction.getEmail().equals(transactionData.getEmail()));
        Assert.assertTrue(detailTransaction.getProduct().equals(transactionData.getProduct()));
        Assert.assertTrue(detailTransaction.getTransactionId().equals(transactionData.getTransactionId()));

    }

    @Test
    public void getByTransactionIdTest(){
        TransactionData transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR00101");
        detailTransactionDao.save(transactionData, "inquire");
        List<DetailTransaction> detailTransactions = detailTransactionDao.getByTransactionId(transactionData.getTransactionId());
        for (DetailTransaction detailTransaction:detailTransactions) {
            Assert.assertNotNull(detailTransaction);
            Assert.assertTrue(detailTransaction.getNominal().equals(transactionData.getNominal()));
            Assert.assertTrue(detailTransaction.getEmail().equals(transactionData.getEmail()));
            Assert.assertTrue(detailTransaction.getProduct().equals(transactionData.getProduct()));
            Assert.assertTrue(detailTransaction.getTransactionId().equals(transactionData.getTransactionId()));
        }
    }

}
