package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoTest {

    @Autowired
    UserDao userDao;

    @Test
    public void insertUserTest(){
        RegisterDto registerDto = new RegisterDto("testong12312@gmail.com", "test1234");
        User user = userDao.insertUser(registerDto);

        System.out.println(user.toString());
        System.out.println("ini time stamp create : "+user.getTimestamp());

        //Positive case
        Assert.assertNotNull(user);

        //Positive case
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNotNull(user.getLastBalance());
        Assert.assertNotNull(user.getTimestamp());

        //positive case
        user = userDao.insertUser(registerDto);
        System.out.println(user.toString());
        Assert.assertTrue(user.getLastBalance() == null);
        Assert.assertTrue(user.getEmail().equals("testong12312@gmail.com"));
        Assert.assertTrue(user.getPassword().equals("test1234"));
        Assert.assertTrue(user.getId() == null);

        //another registration
        registerDto = new RegisterDto("kukuhpradipta@gmail.com", "asdasdsadasdasd");
        user = userDao.insertUser(registerDto);
        System.out.println(user.toString());
        Assert.assertTrue(user.getLastBalance() == 100000L);
        Assert.assertTrue(user.getEmail().equals("kukuhpradipta@gmail.com"));
        Assert.assertTrue(user.getPassword().equals("asdasdsadasdasd"));
    }

    @Test
    public void getUserByEmailTest(){
        RegisterDto registerDto = new RegisterDto("testong@gmail.com", "test1234");
        userDao.insertUser(registerDto);
        registerDto = new RegisterDto("abrakadabra@gmail.com", "pokemonmagic");
        userDao.insertUser(registerDto);
        registerDto = new RegisterDto("monkeykingbar@gmail.com", "antimagic");
        userDao.insertUser(registerDto);


        //positive case
        User user = userDao.getUserByEmail("testong@gmail.com");
        Assert.assertNotNull(user);
        Assert.assertTrue(user.getEmail().equals("testong@gmail.com"));
        Assert.assertTrue(user.getPassword().equals("test1234"));


        //positive case
        user = userDao.getUserByEmail("abrakadabra@gmail.com");
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getEmail().equals("kukuhpradipta@gmail.com"));
        Assert.assertTrue(user.getPassword().equals("pokemonmagic"));


        user = userDao.getUserByEmail("monkeykingbar@gmail.com");
        Assert.assertNotNull(user);
        Assert.assertFalse(user.getEmail().equals("abrakadabra@gmail.com"));
        Assert.assertFalse(user.getPassword().equals("pokemonmagic"));
    }

    @Test
    public void updateBalanceTest(){
        RegisterDto registerDto = new RegisterDto("testong12321ed@gmail.com", "test1234");
        userDao.insertUser(registerDto);
        User u = userDao.updateBalance(registerDto.getEmail(), 11750L);
        System.out.println(u);
        Assert.assertNotNull(u);
        Assert.assertTrue(u.getLastBalance() < 100000L);
        Assert.assertTrue(u.getLastBalance() == 88250L);
    }

    @Test
    public void refundBalanceTest(){
        RegisterDto registerDto = new RegisterDto("teston12312412g@gmail.com", "test1234");
        userDao.insertUser(registerDto);
        User u = userDao.updateBalance(registerDto.getEmail(), 11750L);
        System.out.println(u);
        Assert.assertNotNull(u);
        Assert.assertTrue(u.getLastBalance() < 100000L);
        Assert.assertTrue(u.getLastBalance() == 88250L);

        u = userDao.refundBalance(registerDto.getEmail(), 11750L);
        Assert.assertNotNull(u);
        Assert.assertTrue(u.getLastBalance() == 100000L);
        Assert.assertTrue(u.getLastBalance() - 11750L == 88250L);

    }
}
