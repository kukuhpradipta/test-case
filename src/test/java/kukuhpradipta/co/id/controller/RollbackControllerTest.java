package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.model.User;
import kukuhpradipta.co.id.service.intr.CommitService;
import kukuhpradipta.co.id.service.intr.InquireService;
import kukuhpradipta.co.id.service.intr.PaymentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RollbackControllerTest {

    @Autowired
    RollbackController rollbackController;

    @Autowired
    PaymentService paymentService;

    @Autowired
    InquireService inquireService;

    @Autowired
    ProductDao productDao;

    @Autowired
    UserDao userDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;

    @Autowired
    CommitService commitService;

    @Test
    public void rollbackTest(){
        RegisterDto registerDto = new RegisterDto("testRollback@gmail.com", "test1234");
        User u = userDao.insertUser(registerDto);
        TransactionData transactionData = new TransactionData("E001", "testRollback@gmail.com", "test1234", 10000L, "TR9191","S001", "163yghjknqhwve61723");
        inquireService.inquire(transactionData);
        paymentService.payment(transactionData);

        MasterTransaction masterTransaction = rollbackController.rollback(transactionData);
        System.out.println(masterTransaction);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getState());
        Assert.assertTrue(masterTransaction.getState().equals("rollback"));
        Assert.assertNotNull(masterTransaction.getEmail());
        Assert.assertTrue(masterTransaction.getEmail().equals(transactionData.getEmail()));
        Assert.assertNotNull(masterTransaction.getSupplier());
        Assert.assertTrue(masterTransaction.getSupplier().equals(transactionData.getSupplier()));
        Assert.assertNotNull(masterTransaction.getFee());
        Assert.assertTrue(masterTransaction.getFee() == 1750);
        Assert.assertNotNull(masterTransaction.getTotal());
        Assert.assertTrue(masterTransaction.getTotal() == transactionData.getNominal() + masterTransaction.getFee());
        Assert.assertNotNull(masterTransaction.getLastBalance());
        System.out.println(transactionData);
        System.out.println(masterTransaction);

        transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR00S1","S001", "163yghjknqhwve61723");
        masterTransaction = rollbackController.rollback(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Data Not Found"));

        transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR893","S001", "163yghjknqhwve61723");
        inquireService.inquire(transactionData);
        paymentService.payment(transactionData);
        commitService.commit(transactionData);
        masterTransaction = rollbackController.rollback(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().contains("Transaction Already Successful at"));

    }

}
