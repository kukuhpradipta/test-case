package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.DetailTransaction;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.model.Product;
import kukuhpradipta.co.id.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class InquireControllerTest {

    @Autowired
    InquireController inquireController;

    @Autowired
    UserDao userDao;

    @Autowired
    ProductDao productDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;

    @Test
    public void inquireTest(){

        TransactionData transactionData = new TransactionData("E001", "testonggh@gmail.com", "test1234", 10000L, "TR111","S001", "163yghjknqhwve61723");
        RegisterDto registerDto = new RegisterDto(transactionData.getEmail(), transactionData.getPassword());
        User user = userDao.insertUser(registerDto);
        Product product = productDao.getCheapestProductByProductCode(transactionData.getProduct());
        MasterTransaction masterTransaction = inquireController.inquire(transactionData);
        List<DetailTransaction> detailTransactions = detailTransactionDao.getByTransactionId(transactionData.getTransactionId());
        DetailTransaction detailTransaction = detailTransactions.get(0);
        System.out.println(Arrays.toString(detailTransactions.toArray()));
        Assert.assertNotNull(masterTransaction);
        Assert.assertTrue(masterTransaction.getSupplier().equals(product.getSupplierCode()));
        Assert.assertTrue(masterTransaction.getFee().equals(product.getPrice()));
        Assert.assertTrue(masterTransaction.getToken().equals(product.getToken()));
        Assert.assertTrue(masterTransaction.getState().equals("inquire"));
        Assert.assertNotNull(masterTransaction.getCreatedTimestamp());
        Assert.assertNotNull(masterTransaction.getUpdatedTimestamp());
        Assert.assertTrue(masterTransaction.getCreatedTimestamp().equals(masterTransaction.getUpdatedTimestamp()));
        Assert.assertTrue(masterTransaction.getTotal() == detailTransaction.getNominal() + masterTransaction.getFee());

        Assert.assertNotNull(detailTransaction);
        Assert.assertTrue(detailTransaction.getNominal().equals(transactionData.getNominal()));
        Assert.assertTrue(detailTransaction.getEmail().equals(transactionData.getEmail()));
        Assert.assertTrue(detailTransaction.getProduct().equals(transactionData.getProduct()));
        Assert.assertTrue(detailTransaction.getTransactionId().equals(transactionData.getTransactionId()));

        transactionData = new TransactionData("E001", "testong@gmail.com", "test123412314", 10000L, "TR111");
        masterTransaction = inquireController.inquire(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Your Password Is Not Valid"));

        transactionData = new TransactionData("E001", "testongasdsad@gmail.com", "test123412314", 10000L, "TR111");
        masterTransaction = inquireController.inquire(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Your Account Is Not Valid"));
    }

}
