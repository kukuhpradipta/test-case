package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.model.Product;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductControllerTest {

    @Autowired
    ProductController productController;

    @Test
    public void getProductTest(){
        Product product = productController.getProduct("E001");
        Assert.assertNotNull(product);
        //positive case get value each variable
        Assert.assertNotNull(product.getProduct());
        Assert.assertNotNull(product.getProductCode());
        Assert.assertNotNull(product.getDescription());
        Assert.assertNotNull(product.getPrice());
        Assert.assertNotNull(product.getToken());
        Assert.assertNotNull(product.getSupplierCode());

        Assert.assertTrue(product.getPrice()  == 1750 );
        Assert.assertTrue(product.getSupplierCode().equals("S001"));
        Assert.assertTrue(product.getToken().equals("163yghjknqhwve61723"));

        product = productController.getProduct("A001");
        Assert.assertNull(product);

    }

}
