package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RegisterControllerTest {

    @Autowired
    RegisterController registerController;

    @Test
    public void registerTest(){
        RegisterDto registerDto = new RegisterDto("testongz@gmail.com", "test1234");
        User user = registerController.register(registerDto);
        Assert.assertNotNull(user);

        //Positive case
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNotNull(user.getLastBalance());
        Assert.assertNotNull(user.getTimestamp());

        registerDto = new RegisterDto("testongz@gmail.com", "test1234");
        user = registerController.register(registerDto);
        Assert.assertNotNull(user);

        System.out.println("user : "+user);

        //Positive case
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNull(user.getLastBalance());
        Assert.assertNull(user.getTimestamp());
        Assert.assertTrue(user.getMessage().equals("Failed To Register"));



        registerDto = new RegisterDto("testongzxc@gmail.com", "test1234");
        user = registerController.register(registerDto);
        Assert.assertNotNull(user);

        //Positive case
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNotNull(user.getLastBalance());
        Assert.assertTrue(user.getLastBalance() == 100000L);
        Assert.assertNotNull(user.getTimestamp());
    }

}
