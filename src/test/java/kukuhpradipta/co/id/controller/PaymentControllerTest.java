package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.DetailTransaction;
import kukuhpradipta.co.id.model.MasterTransaction;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentControllerTest {

    @Autowired
    PaymentController paymentController;

    @Autowired
    UserDao userDao;

    @Autowired
    ProductDao productDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;

    @Autowired
    InquireController inquireController;

    @Test
    public void paymentTest(){
        RegisterDto registerDto = new RegisterDto("testong@gmail.com", "test1234");
        userDao.insertUser(registerDto);
        TransactionData transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 10000L, "TR001","S001", "163yghjknqhwve61723");
        MasterTransaction masterTransaction  = inquireController.inquire(transactionData);
        System.out.println(masterTransaction);
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getState());
        Assert.assertTrue(masterTransaction.getState().equals("payment"));
        Assert.assertNotNull(masterTransaction.getEmail());
        Assert.assertTrue(masterTransaction.getEmail().equals(transactionData.getEmail()));
        Assert.assertNotNull(masterTransaction.getSupplier());
        Assert.assertTrue(masterTransaction.getSupplier().equals(transactionData.getSupplier()));
        Assert.assertNotNull(masterTransaction.getFee());
        Assert.assertTrue(masterTransaction.getFee() == 1750);
        Assert.assertNotNull(masterTransaction.getTotal());
        Assert.assertTrue(masterTransaction.getTotal() == transactionData.getNominal() + masterTransaction.getFee());
        Assert.assertNotNull(masterTransaction.getLastBalance());
        Assert.assertTrue(masterTransaction.getLastBalance() < 100000L);

        List<DetailTransaction> detailTransactionList = detailTransactionDao.getByTransactionId(masterTransaction.getTransactionId());
        Assert.assertTrue(detailTransactionList.size() > 0);
        Assert.assertTrue(detailTransactionList.size() > 1);

        transactionData = new TransactionData("S001", "testong@gmail.com", "test1234", 10000L, "TR001","S001", "163yghjknqhwve61723");
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Product Is Different From Inquire Process"));

        transactionData = new TransactionData("E001", "tsestong@gmail.com", "test1234", 10000L, "TR001","S001", "163yghjknqhwve61723");
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Email Is Different From Inquire Process"));

        transactionData = new TransactionData("E001", "testong@gmail.com", "test12314", 10000L, "TR001","S001", "163yghjknqhwve61723");
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Password Is Different From Inquire Process"));

        transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 100001L, "TR001","S001", "163yghjknqhwve61723");
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Nominal Is Different From Inquire Process"));

        transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 100001L, "TR001","B001", "163yghjknqhwve61723");
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Supplier Is Different From Inquire Process"));

        transactionData = new TransactionData("E001", "testong@gmail.com", "test1234", 100001L, "TR001","S001", "1163yghjknqhwve61723");
        masterTransaction = paymentController.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getMessage());
        Assert.assertTrue(masterTransaction.getMessage().equals("Token Is Different From Inquire Process"));
    }

}
