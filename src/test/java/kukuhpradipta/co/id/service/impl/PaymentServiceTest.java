package kukuhpradipta.co.id.service.impl;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.DetailTransaction;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.model.Product;
import kukuhpradipta.co.id.service.intr.InquireService;
import kukuhpradipta.co.id.service.intr.PaymentService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class PaymentServiceTest {

    @Autowired
    PaymentService paymentService;

    @Autowired
    InquireService inquireService;

    @Autowired
    ProductDao productDao;

    @Autowired
    UserDao userDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;

    @Test
    public void paymentTest() {
        RegisterDto registerDto = new RegisterDto("testongzsdas@gmail.com", "test1234");
        userDao.insertUser(registerDto);
        TransactionData transactionData = new TransactionData("E001", "testongzsdas@gmail.com", "test1234", 10000L, "TR0010","S001", "163yghjknqhwve61723");
        MasterTransaction masterTransaction  = inquireService.inquire(transactionData);
        System.out.println(masterTransaction);
        masterTransaction = paymentService.payment(transactionData);
        Assert.assertNotNull(masterTransaction);
        Assert.assertNotNull(masterTransaction.getState());
        Assert.assertTrue(masterTransaction.getState().equals("payment"));
        Assert.assertNotNull(masterTransaction.getEmail());
        Assert.assertTrue(masterTransaction.getEmail().equals(transactionData.getEmail()));
        Assert.assertNotNull(masterTransaction.getSupplier());
        Assert.assertTrue(masterTransaction.getSupplier().equals(transactionData.getSupplier()));
        Assert.assertNotNull(masterTransaction.getFee());
        Assert.assertTrue(masterTransaction.getFee() == 1750);
        Assert.assertNotNull(masterTransaction.getTotal());
        Assert.assertTrue(masterTransaction.getTotal() == transactionData.getNominal() + masterTransaction.getFee());
        Assert.assertNotNull(masterTransaction.getLastBalance());
        Assert.assertTrue(masterTransaction.getLastBalance() < 100000L);

        List<DetailTransaction> detailTransactionList = detailTransactionDao.getByTransactionId(masterTransaction.getTransactionId());
        Assert.assertTrue(detailTransactionList.size() > 0);
        Assert.assertTrue(detailTransactionList.size() > 1);
        Assert.assertFalse(detailTransactionList.size()> 3);


    }
}
