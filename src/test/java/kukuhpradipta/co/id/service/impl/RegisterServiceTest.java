package kukuhpradipta.co.id.service.impl;

import kukuhpradipta.co.id.dto.RegisterDto;
//import kukuhpradipta.co.id.dto.RegisterResponse;
import kukuhpradipta.co.id.model.User;
import kukuhpradipta.co.id.service.intr.RegisterService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RegisterServiceTest {


    @Autowired
    RegisterService registerService;

    @Test
    public void registerTest(){
        RegisterDto registerDto = new RegisterDto("asdaskabrassadas@gmail.com", "test1234");
        User user = registerService.register(registerDto);
        Assert.assertNotNull(user);

        //Positive case
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNotNull(user.getLastBalance());
        Assert.assertNotNull(user.getTimestamp());

        registerDto = new RegisterDto("asdaskabrassadas@gmail.com", "test1234");
        user = registerService.register(registerDto);
        Assert.assertNotNull(user);

        System.out.println("user : "+user);

        //Positive case
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNull(user.getLastBalance());
        Assert.assertNull(user.getTimestamp());
        Assert.assertTrue(user.getMessage().equals("Failed To Register"));



        registerDto = new RegisterDto("testongzxczxs@gmail.com", "test1234");
        user = registerService.register(registerDto);
        Assert.assertNotNull(user);

        //Positive case
        Assert.assertNotNull(user.getId());
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getPassword());
        Assert.assertNotNull(user.getLastBalance());
        Assert.assertTrue(user.getLastBalance() == 100000L);
        Assert.assertNotNull(user.getTimestamp());
    }


}
