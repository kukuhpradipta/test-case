package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@Repository
public class UserDaoI implements UserDao {

    @Autowired
    SessionFactory sessionFactory;

    /*
    * First step this method return empty constructor of user, because test case just need the user ist not null
    * Second step this method return with parameterize constructor of user, because test case want to check each variable value is not null
    * Third step store data into database
    * */
    @Override
    public User insertUser(RegisterDto registerDto) {
        Session session = null;
        User u = new User(registerDto.getPassword(), registerDto.getEmail());
        try {

            session = sessionFactory.openSession();
            session.save(u);
            session.refresh(u);
            System.out.println("u after insert and refresh : "+u.toString());
        } catch (Exception e) {
            return u;
        } finally {
            if (session != null)
                session.close();
        }
        return u;
    }


    /*
    * First step this method return empty constructor of user, because test case just need the user ist not null
    * Second step this method return with parameterize constructor of user, because test case want to check each variable value is not null
    * */
    @Override
    public User getUserByEmail(String username) {
        Session session = null;
        User u = null;

        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("email", username));
            u = (User) criteria.uniqueResult();
        } catch (Exception e) {
//            e.printStackTrace();
            System.out.println("error : "+e.getCause());
        } finally {
            if (session!= null)
                session.close();
        }

        return u;
    }

    @Override
    public User updateBalance(String username, Long amount) {
        Session session = null;
        User u = null;
        Transaction tx= null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("email", username));
            criteria.setLockMode(LockMode.PESSIMISTIC_WRITE);
            u = (User) criteria.uniqueResult();

            u.setLastBalance(u.getLastBalance() - amount);

            session.update(u);
            tx.commit();
            session.refresh(u);
        } catch (Exception e) {
            if (tx != null)
                tx.rollback();
            System.out.println("error : "+e.getCause());
        } finally {
            if (session!= null)
                session.close();
        }

        return u;
    }

    @Override
    public User refundBalance(String username, long amount) {
        Session session = null;
        User u = null;
        Transaction tx= null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            Criteria criteria = session.createCriteria(User.class);
            criteria.add(Restrictions.eq("email", username));
            criteria.setLockMode(LockMode.PESSIMISTIC_WRITE);
            u = (User) criteria.uniqueResult();

            u.setLastBalance(u.getLastBalance() + amount);

            session.update(u);
            tx.commit();
            session.refresh(u);
        } catch (Exception e) {
            if (tx != null)
                tx.rollback();
            System.out.println("error : "+e.getCause());
        } finally {
            if (session!= null)
                session.close();
        }

        return u;
    }
}
