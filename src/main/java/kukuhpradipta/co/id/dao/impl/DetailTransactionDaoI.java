package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.DetailTransaction;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@Repository
public class DetailTransactionDaoI implements DetailTransactionDao{

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public List<DetailTransaction> getByTransactionId(String transactionId) {
        Session session = null;
        List<DetailTransaction> detailTransaction = null;
        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(DetailTransaction.class);
            criteria.add(Restrictions.eq("transactionId", transactionId));
            detailTransaction = criteria.list();
        } catch (HibernateException e) {
            e.printStackTrace();
            return detailTransaction;
        } finally {
            if (session!=null)
                session.close();
        }

        return detailTransaction;
    }

    @Override
    public DetailTransaction save(TransactionData transactionData, String state) {
        Session session = null;
        DetailTransaction detailTransaction = new DetailTransaction(transactionData.getProduct(), transactionData.getEmail(), transactionData.getNominal(), transactionData.getTransactionId(), transactionData.getSupplier(), transactionData.getToken(), state);

        try {
            session = sessionFactory.openSession();
            session.save(detailTransaction);
            session.refresh(detailTransaction);
        } catch (Exception e) {
            e.printStackTrace();
            return detailTransaction;
        } finally {
            if (session!=null)
                session.close();
        }

        return detailTransaction;
    }
}
