package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.MasterTransactionDao;
import kukuhpradipta.co.id.model.MasterTransaction;
import org.hibernate.*;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@Repository
public class MasterTransactionDaoI implements MasterTransactionDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public MasterTransaction save(String transactionId, String email, String supplier, String token, String state, Long fee, Long total, String password, String product, Long nominal) {
        Session session = null;
        MasterTransaction masterTransaction = new MasterTransaction(transactionId, email, supplier, token, state, fee, total, password, product, nominal);

        try {
            session = sessionFactory.openSession();
            session.save(masterTransaction);
            session.refresh(masterTransaction);
            System.out.println("after refresh : "+masterTransaction.toString());
        } catch (Exception e) {
            return masterTransaction;
        } finally {
            if (session != null)
                session.close();
        }

        return masterTransaction;

    }

    @Override
    public MasterTransaction findByTransactionId(String transactionId) {
        Session session = null;
        MasterTransaction masterTransaction = null;

        try {
            session = sessionFactory.openSession();
            Criteria criteria = session.createCriteria(MasterTransaction.class);
            criteria.add(Restrictions.eq("transactionId", transactionId));

            masterTransaction = (MasterTransaction) criteria.uniqueResult();
        } catch (HibernateException e) {
            e.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }

        return  masterTransaction;
    }

    @Override
    public MasterTransaction update(MasterTransaction masterTransaction) {
        Session session = null;
        Transaction tx = null;
        try {
            session = sessionFactory.openSession();
            tx = session.beginTransaction();
            session.update(masterTransaction);
            tx.commit();
            session.refresh(masterTransaction);
            System.out.println("after refresh : "+masterTransaction.toString());
        } catch (Exception e) {
            if (tx != null) tx.rollback();
            return masterTransaction;
        } finally {
            if (session != null)
                session.close();
        }

        return masterTransaction;
    }
}
