package kukuhpradipta.co.id.dao.impl;

import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.model.Product;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@Repository
public class ProductDaoI implements ProductDao{

    @Autowired
    SessionFactory sessionFactory;

    /*
    * First step this method return empty constructor of product, because test case just need the product ist not null
    * Second step this method return with parameterize constructor of product, because test case want to check each variable value is not null
    * Third step this method return with parameterize constructor of product, but data is from database
    * */
    @Override
    public Product getCheapestProductByProductCode(String productCode) {
        Session session = sessionFactory.openSession();
        Product product = null;
        try {

            Criteria criteria = session.createCriteria(Product.class);
            criteria.add(Restrictions.eq("productCode", productCode));
            criteria.addOrder(Order.asc("price"));
            criteria.addOrder(Order.asc("supplierCode"));
            criteria.setMaxResults(1);
            product = (Product) criteria.uniqueResult() ;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }
        return product;
    }

    @Override
    public Product getProductByProductCodeAndSupplier(String productCode, String supplierCode) {
        Session session = sessionFactory.openSession();
        Product product = null;
        try {
            Criteria criteria = session.createCriteria(Product.class);
            criteria.add(Restrictions.eq("productCode", productCode));
            criteria.add(Restrictions.eq("supplierCode", supplierCode));
            criteria.setMaxResults(1);
            product = (Product) criteria.uniqueResult() ;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (session != null)
                session.close();
        }
        return product;
    }
}
