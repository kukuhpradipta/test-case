package kukuhpradipta.co.id.dao.intr;

import kukuhpradipta.co.id.model.Product;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
public interface ProductDao {
     Product getCheapestProductByProductCode(String productCode);


     Product getProductByProductCodeAndSupplier(String productCode, String supplierCode);
}
