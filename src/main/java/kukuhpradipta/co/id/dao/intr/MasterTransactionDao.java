package kukuhpradipta.co.id.dao.intr;

import kukuhpradipta.co.id.model.MasterTransaction;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
public interface MasterTransactionDao {
    MasterTransaction save(String transactionId, String email, String supplier, String token, String state, Long fee, Long total, String password, String product, Long nominal);

    MasterTransaction findByTransactionId(String transactionId);

    MasterTransaction update(MasterTransaction masterTransaction);
}
