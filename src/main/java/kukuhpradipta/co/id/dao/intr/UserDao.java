package kukuhpradipta.co.id.dao.intr;

import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
public interface UserDao {
    User insertUser(RegisterDto registerDto);

    User getUserByEmail(String username);

    User updateBalance(String username, Long amount);

    User refundBalance(String username, long l);
}
