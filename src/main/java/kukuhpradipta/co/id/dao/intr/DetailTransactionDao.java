package kukuhpradipta.co.id.dao.intr;

import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.DetailTransaction;

import java.util.List;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
public interface DetailTransactionDao {

    DetailTransaction save(TransactionData transactionData, String state);

    List<DetailTransaction> getByTransactionId(String transactionId);
}
