package kukuhpradipta.co.id.service.impl;

import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.model.Product;
import kukuhpradipta.co.id.service.intr.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@Service
public class ProductServiceI implements ProductService {

    @Autowired
    ProductDao productDao;

    @Override
    public Product getCheapestProductByProductCode(String productCode) {
        return productDao.getCheapestProductByProductCode(productCode);
    }
}
