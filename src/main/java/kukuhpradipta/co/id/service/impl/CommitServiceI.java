package kukuhpradipta.co.id.service.impl;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.MasterTransactionDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.service.intr.CommitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@Service
public class CommitServiceI implements CommitService{


    @Autowired
    MasterTransactionDao masterTransactionDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;


    @Override
    public MasterTransaction commit(TransactionData transactionData) {
        MasterTransaction masterTransaction = masterTransactionDao.findByTransactionId(transactionData.getTransactionId());

        if (masterTransaction == null) {
            masterTransaction = new MasterTransaction();
            masterTransaction.setMessage("Data Not Found");
            return masterTransaction;
        }

        if (masterTransaction.getState().equals("rollback")) {
            masterTransaction = new MasterTransaction();
            masterTransaction.setMessage("Transaction Already Cancelled at "+masterTransaction.getUpdatedTimestamp());
            return masterTransaction;
        }

        masterTransaction.setState("commit");
        masterTransaction.setUpdatedTimestamp(new Timestamp(Calendar.getInstance().getTime().getTime()));
        masterTransaction = masterTransactionDao.update(masterTransaction);
        TransactionData transactionData2 =  new TransactionData(masterTransaction.getProduct(), masterTransaction.getEmail(), masterTransaction.getPassword(), masterTransaction.getNominal(), masterTransaction.getTransactionId(),masterTransaction.getSupplier(), masterTransaction.getToken());
        detailTransactionDao.save(transactionData2, masterTransaction.getState());

        return masterTransaction;
    }
}
