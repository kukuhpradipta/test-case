package kukuhpradipta.co.id.service.impl;

import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.MasterTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.model.Product;
import kukuhpradipta.co.id.model.User;
import kukuhpradipta.co.id.service.intr.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@Service
public class PaymentServiceI implements PaymentService {

    @Autowired
    MasterTransactionDao masterTransactionDao;

    @Autowired
    UserDao userDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;


    @Override
    public MasterTransaction payment(TransactionData transactionData) {
        MasterTransaction masterTransaction = masterTransactionDao.findByTransactionId(transactionData.getTransactionId());
        try {
            System.out.println(masterTransaction);
            if (!transactionData.getProduct().equals(masterTransaction.getProduct())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Product Is Different From Inquire Process");
                return masterTransaction;
            }

            if (!transactionData.getSupplier().equals(masterTransaction.getSupplier())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Supplier Is Different From Inquire Process");
                return masterTransaction;
            }

            if (!transactionData.getProduct().equals(masterTransaction.getProduct())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Product Is Different From Inquire Process");
                return masterTransaction;
            }

            if (!transactionData.getToken().equals(masterTransaction.getToken())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Token Is Different From Inquire Process");
                return masterTransaction;
            }

            if (!transactionData.getEmail().equals(masterTransaction.getEmail())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Email Is Different From Inquire Process");
                return masterTransaction;
            }

            if (!transactionData.getPassword().equals(masterTransaction.getPassword())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Password Is Different From Inquire Process");
                return masterTransaction;
            }

            if (!transactionData.getNominal().equals(masterTransaction.getNominal())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Nominal Is Different From Inquire Process");
                return masterTransaction;
            }
            User u = userDao.updateBalance(transactionData.getEmail(), masterTransaction.getTotal());
            masterTransaction.setLastBalance(u.getLastBalance());
            masterTransaction.setState("payment");
            masterTransaction.setUpdatedTimestamp(new Timestamp(Calendar.getInstance().getTime().getTime()));
            masterTransaction = masterTransactionDao.update(masterTransaction);
            detailTransactionDao.save(transactionData, masterTransaction.getState());
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error "+e.getMessage());
        }


        return masterTransaction;
    }
}
