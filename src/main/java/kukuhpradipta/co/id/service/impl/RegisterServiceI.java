package kukuhpradipta.co.id.service.impl;

import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;
import kukuhpradipta.co.id.service.intr.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */

@Service
public class RegisterServiceI implements RegisterService {

    @Autowired
    UserDao userDao;

    @Override
    public User register(RegisterDto registerDto) {
        User user = null;
        try {
            user = userDao.insertUser(registerDto);

            if (user.getId() == null)
                user.setMessage("Failed To Register");

        } catch (Exception e) {
            System.out.println("error : "+e.getCause());
            return user;
        }
        return user;
    }
}
