package kukuhpradipta.co.id.service.impl;

import com.google.gson.Gson;
import kukuhpradipta.co.id.dao.intr.DetailTransactionDao;
import kukuhpradipta.co.id.dao.intr.MasterTransactionDao;
import kukuhpradipta.co.id.dao.intr.ProductDao;
import kukuhpradipta.co.id.dao.intr.UserDao;
import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.model.Product;
import kukuhpradipta.co.id.model.User;
import kukuhpradipta.co.id.service.intr.InquireService;
import kukuhpradipta.co.id.utils.HttpConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@Service
public class InquireServiceI implements InquireService{

    @Autowired
    ProductDao productDao;

    @Autowired
    MasterTransactionDao masterTransactionDao;

    @Autowired
    DetailTransactionDao detailTransactionDao;

    @Autowired
    UserDao userDao;

    @Override
    public MasterTransaction inquire(TransactionData transactionData) {
        MasterTransaction masterTransaction = null;
        try {
            User user = userDao.getUserByEmail(transactionData.getEmail());

            if (user == null) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Your Account Is Not Valid");
                return masterTransaction;
            }
            if (!user.getPassword().equals(transactionData.getPassword())) {
                masterTransaction = new MasterTransaction();
                masterTransaction.setMessage("Your Password Is Not Valid");
                return masterTransaction;
            }

            Product product = productDao.getCheapestProductByProductCode(transactionData.getProduct());
//            String res = new HttpConnection().connect("http://localhost:8080/product/"+transactionData.getProduct(), "", false);
//            Product product = new Gson().fromJson(res, Product.class);
            masterTransaction = masterTransactionDao.save(transactionData.getTransactionId(),transactionData.getEmail(),product.getSupplierCode(),product.getToken(),"inquire",product.getPrice(),transactionData.getNominal() + product.getPrice(), transactionData.getPassword(), transactionData.getProduct(), transactionData.getNominal());
            detailTransactionDao.save(transactionData, masterTransaction.getState());

        } catch (Exception e) {
            System.out.println("Error "+e.getMessage());
        }


        return masterTransaction;
    }


}
