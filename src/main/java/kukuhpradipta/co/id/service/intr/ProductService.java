package kukuhpradipta.co.id.service.intr;

import kukuhpradipta.co.id.model.Product;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
public interface ProductService {
    Product getCheapestProductByProductCode(String productCode);
}
