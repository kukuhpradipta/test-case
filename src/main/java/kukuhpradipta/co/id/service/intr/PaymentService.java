package kukuhpradipta.co.id.service.intr;

import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */

public interface PaymentService {
    MasterTransaction payment(TransactionData transactionData);
}
