package kukuhpradipta.co.id.service.intr;

import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
public interface RegisterService {
    User register(RegisterDto registerDto);
}
