package kukuhpradipta.co.id.model;

import org.hibernate.annotations.ColumnDefault;
import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@Entity
@Table(name = "m_user", uniqueConstraints = @UniqueConstraint(name = "unique_email", columnNames = "email"))
@DynamicInsert
public class User {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String email;
    @Column
    private String password;
    @Column(columnDefinition = "bigint default 100000")
    private Long lastBalance;
    @Column(columnDefinition = "timestamp default CURRENT_TIMESTAMP")
    private Timestamp timestamp;

    private String message;
    /*
     * Non parameter constructor is needed when we need to create a object without set variable value
     * */
    public User() {
    }

    public User(String password, String email) {
        this.password = password;
        this.email = email;
    }

    /*
        * Set value at the constructor to made it this class more simple.
        *
        * */
    public User(String email, String password, Long lastBalance, Timestamp timestamp) {
        this.email = email;
        this.password = password;
        this.lastBalance = lastBalance;
        this.timestamp = timestamp;
    }

    /*
    * Simple getter for get the value after we set it
    *
    */


    /*
    *getProduct function to get the id value
    * */
    public Long getId() {
        return id;
    }

    /*
    *getProduct function to get the email value
    * */
    public String getEmail() {
        return email;
    }


    /*
    *getProduct function to get password id value
    * */
    public String getPassword() {
        return password;
    }

    /*
    *getProduct function to get the balance value
    * */
    public Long getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(Long lastBalance) {
        this.lastBalance = lastBalance;
    }

    /*


        *getProduct function to get the timestamp value
        * */
    public Timestamp getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", lastBalance=" + lastBalance +
                ", timestamp=" + timestamp +
                ", message='" + message + '\'' +
                '}';
    }
}
