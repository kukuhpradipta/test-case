package kukuhpradipta.co.id.model;

import com.google.gson.annotations.SerializedName;

import javax.persistence.*;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@Entity
@Table(name = "m_product")
public class Product {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)

    private Long id;
    @Column
    @SerializedName("productName")
    private String product;
    @Column
    @SerializedName("product")
    private String productCode;
    @Column
    private Long price;
    @Column
    private String description;
    @Column
    private String token;
    @Column
    @SerializedName("supplier")
    private String supplierCode;


     /*
     * Non parameter constructor is needed when we need to create a object without set variable value
     * */
    public Product() {
    }


    /*
    * Set value at the constructor to made it this class more simple.
    *
    * */
    public Product(String product, String productCode, Long price, String description, String token, String supplierCode) {
        this.product = product;
        this.productCode = productCode;
        this.price = price;
        this.description = description;
        this.token = token;
        this.supplierCode = supplierCode;
    }




    /*
    * New Variable id to support auto increament database
    * */
    public Long getId() {
        return id;
    }

    /*
     * Simple getter for get the value after we set it
     */
    /*getProduct function to get the product value*/
    public String getProduct() {
        return product;
    }

    /*getProductCode function to get the product code value*/
    public String getProductCode() {
        return productCode;
    }

    /*getProduct function to get the price value, varibale price should using Long type to handle more than 2bil value and null value*/
    public Long getPrice() {
        return price;
    }
    /*getProduct function to get the description value*/
    public String getDescription() {
        return description;
    }

    /*getProduct function to get the token value*/
    public String getToken() {
        return token;
    }

    public String getSupplierCode() {
        return supplierCode;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", product='" + product + '\'' +
                ", productCode='" + productCode + '\'' +
                ", price=" + price +
                ", description='" + description + '\'' +
                ", token='" + token + '\'' +
                ", supplierCode='" + supplierCode + '\'' +
                '}';
    }
}
