package kukuhpradipta.co.id.model;

import org.hibernate.annotations.DynamicInsert;

import javax.persistence.*;
import java.sql.Time;
import java.sql.Timestamp;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
@Entity
@Table(name = "t_master_transaction", uniqueConstraints = @UniqueConstraint(name = "UNIQUE_TRANSACTION_ID", columnNames = "transactionId"))
@DynamicInsert
public class MasterTransaction {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String transactionId;
    @Column
    private String email;
    @Column
    private String supplier;
    @Column
    private String token;
    @Column
    private String state;
    @Column
    private Long fee;
    @Column
    private Long total;
    @Column
    private String password;
    @Column(columnDefinition = "timestamp default CURRENT_TIMESTAMP")
    private Timestamp createdTimestamp;
    @Column(columnDefinition="timestamp default CURRENT_TIMESTAMP")
    private Timestamp updatedTimestamp;
    private Long lastBalance;
    private String message;
    @Column
    private String product;
    @Column
    private Long nominal;

    public MasterTransaction() {
    }

    public MasterTransaction(String transactionId, String email, String supplier, String token, String state, Long fee, Long total, String password, String product, Long nominal) {
        this.transactionId = transactionId;
        this.email = email;
        this.supplier = supplier;
        this.token = token;
        this.state = state;
        this.fee = fee;
        this.total = total;
        this.password = password;
        this.product = product;
        this.nominal = nominal;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getEmail() {
        return email;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getToken() {
        return token;
    }

    public String getState() {
        return state;
    }

    public Long getFee() {
        return fee;
    }

    public Long getTotal() {
        return total;
    }

    public Long getId() {
        return id;
    }

    public Timestamp getCreatedTimestamp() {
        return createdTimestamp;
    }

    public Timestamp getUpdatedTimestamp() {
        return updatedTimestamp;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setUpdatedTimestamp(Timestamp updatedTimestamp) {
        this.updatedTimestamp = updatedTimestamp;
    }

    public Long getLastBalance() {
        return lastBalance;
    }

    public String getMessage() {
        return message;
    }

    public String getPassword() {
        return password;
    }

    public String getProduct() {
        return product;
    }

    public Long getNominal() {
        return nominal;
    }

    public void setLastBalance(Long lastBalance) {
        this.lastBalance = lastBalance;
    }

    @Override
    public String
    toString() {
        return "MasterTransaction{" +
                "id=" + id +
                ", transactionId='" + transactionId + '\'' +
                ", email='" + email + '\'' +
                ", supplier='" + supplier + '\'' +
                ", token='" + token + '\'' +
                ", state='" + state + '\'' +
                ", fee=" + fee +
                ", total=" + total +
                ", password='" + password + '\'' +
                ", createdTimestamp=" + createdTimestamp +
                ", updatedTimestamp=" + updatedTimestamp +
                ", lastBalance=" + lastBalance +
                ", message='" + message + '\'' +
                ", product='" + product + '\'' +
                ", nominal=" + nominal +
                '}';
    }
}
