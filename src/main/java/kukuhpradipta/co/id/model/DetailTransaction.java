package kukuhpradipta.co.id.model;

import javax.persistence.*;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@Entity
@Table(name = "t_detail_transaction")
public class DetailTransaction {

    @Id
    @Column
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String product;
    @Column
    private String email;
    @Column
    private Long nominal;
    @Column
    private String transactionId;
    @Column
    private String supplier;
    @Column
    private String token;
    @Column
    private String state;



    public DetailTransaction() {
    }

    public DetailTransaction(String product, String email, Long nominal, String transactionId, String supplier, String token, String state) {
        this.product = product;
        this.email = email;
        this.nominal = nominal;
        this.transactionId = transactionId;
        this.supplier = supplier;
        this.token = token;
        this.state =state;
    }

    public Long getId() {
        return id;
    }

    public String getProduct() {
        return product;
    }

    public String getEmail() {
        return email;
    }

    public Long getNominal() {
        return nominal;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "DetailTransaction{" +
                "id=" + id +
                ", product='" + product + '\'' +
                ", email='" + email + '\'' +
                ", nominal=" + nominal +
                ", transactionId='" + transactionId + '\'' +
                ", supplier='" + supplier + '\'' +
                ", token='" + token + '\'' +
                '}';
    }
}
