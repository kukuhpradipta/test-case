package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dto.RegisterDto;
import kukuhpradipta.co.id.model.User;
import kukuhpradipta.co.id.service.intr.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RestController
public class RegisterController {

    @Autowired
    RegisterService registerService;

    @RequestMapping(value = "/user/register", method = RequestMethod.PUT)
    public @ResponseBody User register(@RequestBody  RegisterDto registerDto) {
        return registerService.register(registerDto);
    }
}
