package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.service.intr.RollbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RestController
public class RollbackController {

    @Autowired
    RollbackService rollbackService;


    @RequestMapping(value = "/transaction/rollback", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody  MasterTransaction rollback(@RequestBody  TransactionData transactionData) {
        return rollbackService.rollback(transactionData);
    }
}
