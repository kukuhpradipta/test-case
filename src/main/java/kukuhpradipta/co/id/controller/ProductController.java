package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.model.Product;
import kukuhpradipta.co.id.service.intr.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RestController
public class ProductController {

    @Autowired
    ProductService productService;

    @RequestMapping(value = "/product/{proudctCode}", method = RequestMethod.GET)
    public @ResponseBody  Product getProduct(@PathVariable String proudctCode) {
        return productService.getCheapestProductByProductCode(proudctCode);
    }
}
