package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.service.intr.InquireService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RestController
public class InquireController {

    @Autowired
    InquireService inquireService;

    @RequestMapping(value = "/transaction/inquire", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody  MasterTransaction inquire(@RequestBody  TransactionData transactionData) {
        return inquireService.inquire(transactionData);
    }
}
