package kukuhpradipta.co.id.controller;

import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.service.intr.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */
@RestController
public class PaymentController {

    @Autowired
    PaymentService paymentService;

    @RequestMapping(value = "/transaction/payment", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody  MasterTransaction payment(@RequestBody  TransactionData transactionData) {
        return paymentService.payment(transactionData);
    }
}
