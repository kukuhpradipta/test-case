package kukuhpradipta.co.id.controller;

/**
 * Created by Widyana K Pradipta on 11/27/2017.
 */

import kukuhpradipta.co.id.dto.TransactionData;
import kukuhpradipta.co.id.model.MasterTransaction;
import kukuhpradipta.co.id.service.intr.CommitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class CommitController {

    @Autowired
    CommitService commitService;

    @RequestMapping(value = "/transaction/commit", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    MasterTransaction commit(@RequestBody TransactionData transactionData) {
        return commitService.commit(transactionData);
    }
}
