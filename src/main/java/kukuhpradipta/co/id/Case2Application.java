package kukuhpradipta.co.id;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.orm.jpa.vendor.HibernateJpaSessionFactoryBean;

import javax.persistence.EntityManagerFactory;

@SpringBootApplication
@EnableAutoConfiguration
public class Case2Application extends SpringBootServletInitializer {

	public static void main(String[] args) {
		System.out.println(">>>>>>>>>>>>>>>>>. args "+args);
		SpringApplication.run(Case2Application.class, args);
	}

	@Bean
	public HibernateJpaSessionFactoryBean sessionFactory(EntityManagerFactory emf) {
		HibernateJpaSessionFactoryBean fact = new HibernateJpaSessionFactoryBean();
		fact.setEntityManagerFactory(emf);
		return fact;
	}


	@Bean
	public GsonHttpMessageConverter messageConverter(){
		return new GsonHttpMessageConverter();
	}

	@Bean
	public GsonHttpMessageConverter jsonMessageConverter(){
		return new GsonHttpMessageConverter();
	}
}
