package kukuhpradipta.co.id.utils;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/**
 * Created by Widyana K Pradipta on 11/30/2016.
 */
public class HttpConnection {

    private static Logger logger = Logger.getLogger(HttpConnection.class);

    public String connect(String url, String data, boolean encode) throws Exception {
//        JSONObject
        StringBuilder response = new StringBuilder();
        logger.error("[connect] (SEND) - Request Data = " + data + " || URL = " + url);

        try {
            BufferedReader rd;
            String line;
            if (encode) {
                data = URLEncoder.encode(data, "UTF-8");
            }
            HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
            if (data.equals(""))
                con.setRequestMethod("GET");
            else {
                con.setRequestMethod("POST");
                con.setUseCaches(false);
                con.setDoInput(true);
                con.setDoOutput(true);

                con.setRequestProperty("Content-Length", "" + Integer.toString(data.getBytes().length));
                con.setRequestProperty("Content-Language", "en-US");
                con.setRequestProperty("Content-Type", "text/plain");
                con.setDoOutput(true);

                // Send request
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(data);
                wr.flush();
                wr.close();

            }
            System.out.println(con.getRequestMethod());

            String code = String.valueOf(con.getResponseCode());
            // Get Response
            InputStream is = con.getInputStream();
            rd = new BufferedReader(new InputStreamReader(is));


            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\r');
            }

            rd.close();

            logger.info("[connect] (RECEIVE) - Request Data = " + data + " || URL = " + url + " || Response = " + response.toString().trim());
        } catch (Exception e) {
            logger.error(e);
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return response.toString().trim();
    }

}
