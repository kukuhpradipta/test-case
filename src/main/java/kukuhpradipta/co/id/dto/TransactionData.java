package kukuhpradipta.co.id.dto;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
public class TransactionData {

    private String product;
    private String email;
    private String password;
    private Long nominal;
    private String transactionId;
    private String supplier;
    private String token;


    public TransactionData() {
    }

    public TransactionData(String product, String email, String password, Long nominal, String transactionId) {
        this.product = product;
        this.email = email;
        this.password = password;
        this.nominal = nominal;
        this.transactionId = transactionId;
    }

    public TransactionData(String product, String email, String password, Long nominal, String transactionId, String supplier, String token) {
        this.product = product;
        this.email = email;
        this.password = password;
        this.nominal = nominal;
        this.transactionId = transactionId;
        this.supplier = supplier;
        this.token = token;
    }

    public String getProduct() {
        return product;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Long getNominal() {
        return nominal;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getSupplier() {
        return supplier;
    }

    public String getToken() {
        return token;
    }

    @Override
    public String toString() {
        return "TransactionData{" +
                "product='" + product + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", nominal=" + nominal +
                ", transactionId='" + transactionId + '\'' +
                '}';
    }
}
