package kukuhpradipta.co.id.dto;

/**
 * Created by Widyana K Pradipta on 11/26/2017.
 */
public class RegisterDto {
    private String email;
    private String password;

    public RegisterDto() {
    }

    public RegisterDto(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
