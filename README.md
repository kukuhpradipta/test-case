#Case 2.
Created using spring boot, spring runner for test, and H2 for database

#How To Run
    java -jar -Dserver.port=<portnumber> <filename>

#Postman Documentation.
https://documenter.getpostman.com/view/485855/case2/7EDADyA#50f45cc4-8ede-639d-4f0d-980fdf7ad1aa

#Build
mvn clean install -X.

#REST API Mapping

| Path                      | Description                   | Type  |
| --------------------------|:-----------------------------:| -----:|
| /user/register            | Service for user to register  |  POST |
| /transaction/inquire      | inquire transaction data      |  POST |
| /transaction/payment      | pay the transaction           |  PUT  |
| /transaction/commit       | commit the transaction        |  PUT  |
| /transaction/rollback     | cancel the transaction        |  PUT  |
| /product/{procuctCode}    | get the cheapest product data |  GET  |
